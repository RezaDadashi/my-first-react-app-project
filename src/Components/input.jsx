import React from 'react'

export default function Input({ name, label, value, type, onChange, errors, ...rest}) {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
        <input
          value={value} 
          type={type} 
          id={name}
          name={name} 
          className="form-control" 
          onChange={onChange}
          autoFocus={rest.autoFocus}
          placeholder={rest.placeholder}/>
          { type === 'email' 
            ? <small 
              id="emailHelp" 
              className="form-text text-muted"
              >
                We'll never share your email with anyone else.
              </small> 
            : null }

          {errors && <div className='alert alert-danger mt-3'>{errors}</div>}
    </div>
  )
}
