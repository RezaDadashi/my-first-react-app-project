import React from 'react';
import Joi from 'joi-browser';
import Form from './form';
import Input from './input'

export default class Login extends Form {

  state = {
    data : { username: '', password: '' },
    errors : { username: '', password: '' }
  };

  schema = {
    username: Joi.string().email().required(),
    password: Joi.string().min(3).max(10).required()
  }
  
  doSubmit = () => {
    // Call the Server:
    console.log('login form submited');
  }

  render() {
    const { data, errors } = this.state;
    return (
      <div className='container text-center w-50'>
       <h1 className='p-5'>Login Form</h1>
         <form onSubmit={this.handleSubmit}>
           <Input
            value={data.username}
            onChange={this.handleChange} 
            name='username' 
            label='User Name'
            type="email" 
            errors={errors.username}
            autoFocus 
            placeholder='name@example.com'/>
          <Input 
            value={data.password}
            onChange={this.handleChange}
            name='password'
            label='Password'
            type="password"
            errors={errors.password}/>
        
          <button 
            type="submit"
            disabled={this.onSubmitValidation()} 
            className="btn btn-primary m-5 px-5"
          >
            Submit
          </button>
        </form>
    </div>
    )
  }
}