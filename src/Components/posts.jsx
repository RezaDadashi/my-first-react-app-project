import React, { useState , useEffect} from 'react'
import { Route, Switch, Link } from 'react-router-dom';
import axios from 'axios';
import _ from 'lodash';
import Table from './table';
import Category from './category';
import Pagination from './pagination';
import NewPost from './newPost';
import UpdatePost from './updatePost';
import paginate, { startPosition } from '../utility/paginate';

const apiEndPoint = 'https://jsonplaceholder.typicode.com/posts';

export default function Posts({ match }) {
  const categoriesInitial = [
    {title:'All Posts', active: true},
    {title:'Most Visited', active: false},
    {title:'More Likes', active: false},
    {title:'Last Created', active: false},
    {title:'Last Updated', active: false}
  ];

  // --------------- useState Section: ---------------

  const [allPosts, setAllPosts] = useState([]);
  const [categories, setCategories] = useState(categoriesInitial);
  const [isLoading, setIsLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize] = useState(5);
  const [sortOrder, setSortOrder] = useState('asc');
  const [search, setSearch] = useState('');

  // --------------- useEffect Section: ---------------

  useEffect(() => {
    async function fetchData(){
      try {
        const { data: allPosts } = await axios.get(apiEndPoint);
        allPosts.map(post => post.like = false);
        setAllPosts(allPosts);
        setIsLoading(false);
      } catch (error) {
        alert('an error has bean acourd!'); 
      }
    }
    fetchData();
  }, []);
  
  // --------------- Helper Methods Section: ---------------

  const handleCategory = category => {
    const clonedCategories = [...categories];
    clonedCategories.forEach( element => {
      element.active = false;
    });
    const indexOf = categories.indexOf(category);
    clonedCategories[indexOf].active = true;
    setCategories(clonedCategories);
  }
  
  const handlePagination = pageNumber => {
    setCurrentPage(pageNumber);
  }

  const handleAddNewPost = async newPostObj => {
    const { data: post } = await axios.post(apiEndPoint, newPostObj);
    setAllPosts([post, ...allPosts]);
  }
   
  const handleUpdate = async updatedPostObj => {
    const originalAllPosts = [...allPosts];
    // you should use reduce or sum method insted but now just leave it as it is:
    // eslint-disable-next-line eqeqeq
    const originalPost = originalAllPosts.filter(p => p.id == updatedPostObj.id)[0];
    const indexOf = originalAllPosts.indexOf(originalPost);
    allPosts[indexOf] = {...updatedPostObj};
    setAllPosts(allPosts);
    
    try {
      await axios.put(`${apiEndPoint}/${updatedPostObj.id}`, updatedPostObj);
    } catch (error) {
      alert('something goes wrong when updating post!');
      setAllPosts(originalAllPosts);
    }
  }

  const handleLike = post => {
    const originalAllPosts = [...allPosts];
    const indexOf = originalAllPosts.indexOf(post);
    allPosts[indexOf] = {...originalAllPosts[indexOf]};
    allPosts[indexOf].like = !originalAllPosts[indexOf].like;
    // console.log(allPosts, originalAllPosts);
    setAllPosts(allPosts);
  }

  const handleDelete = async post => {
    const originalAllPosts = [...allPosts];
    setAllPosts(allPosts.filter(p => p.id !== post.id));

    try {
      await axios.delete(`${apiEndPoint}/${post.id}`)
    } catch (error) {
      alert('some thing goes wrong for deleting post!');
      setAllPosts(originalAllPosts);
    }
  }

  const handleSort = () => {
    setSortOrder(sortOrder === 'asc' ? 'desc' : 'asc');
  }

  const handleSearch = e => {
    setSearch(e.target.value);
    setCurrentPage(1);
  }
  
  // --------------- Start & Posts: ---------------

  const start = startPosition(pageSize, currentPage);

  let filterd = allPosts;

  if (search) filterd = allPosts.filter(p => 
    p.title.toLowerCase().startsWith(search.toLowerCase()));

  const sorted =  _.orderBy(filterd, ['title'], [sortOrder]);

  const posts = paginate(sorted, pageSize, currentPage);


  // --------------- Render Section: ---------------

  return (
    <>
      <Switch>
        <Route
          path='/posts/new-post'
          render={(props) =>
            <NewPost
              addNewPost={newPostObj => handleAddNewPost(newPostObj)}
              {...props}
            />}
        />
        <Route
          path='/posts/:id'
          render={(props) =>
            <UpdatePost
              updatePost={updatedPostObj => handleUpdate(updatedPostObj)}
              posts={allPosts}
              {...props}
            />}
        />
      </Switch>

      {match.isExact && 
        <div className="container container-fluid-sm pt-4">
          <div className="row">
            <div className="col-xl-3 mr-xl-4">
              <div className="card mb-3">
                <h5 className="card-header">Categories</h5>
              </div>
              <Category categories={categories} onClick={handleCategory}/>
              <Link 
                to='/posts/new-post' 
                className='btn btn-outline-secondary w-100 mt-5 mb-5'
              >
                Add New Post
              </Link>
            </div>
            <div className="col ml-xl-4">
              <h6
                className='text-warning font-weight-light pb-2'>
                Injoy {filterd.length} posts powered by JSON-Placeholder...
              </h6>
              <div class="input-group mb-3">
                <input 
                  type="text" 
                  autoFocus
                  value={search}
                  onChange={handleSearch} 
                  className="form-control" 
                  placeholder="Search..."/>
              </div>
              <Table
                posts={posts}
                start={start}
                isLoading={isLoading}
                onLike={handleLike}
                onDelete={handleDelete}
                onSort={handleSort}
                sortOrder={sortOrder}
              />
              <Pagination
                onClick={handlePagination}
                length={filterd.length}
                pageSize={pageSize}
                currentPage={currentPage}
              />
            </div>
          </div>
        </div>}
    </>
  )
}
