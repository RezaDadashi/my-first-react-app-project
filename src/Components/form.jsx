import { Component } from 'react';
import Joi from 'joi-browser';

export default class Form extends Component {

  state = {
    data: {},
    errors: {}
  }

  onChangeValidation = input => {
    const { name , value } = input.target;

    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const {error} = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;

    // if ( name === 'username' ) 
    //   if ( value.trim() === '' ) return 'User Name is Requierd!'  
    // if ( name === 'password' ) 
    //   if ( value.trim() === '' ) return 'Password is Requierd!'
  }

  onSubmitValidation = () => {
    const { data } = this.state;

    const {error} = Joi.validate(data, this.schema, {abortEarly: false});
    
    if (!error) return null;
    
    const errors = {};
    for (let item of error.details) 
      errors[item.path[0]] = item.message;
    
    return errors;

    // const errors = {};
    // if (data.username.trim() === '') errors.username = 'User Name is Requierd!';
    // if (data.password.trim() === '') errors.password = 'Password is Requierd!';
    // return errors;
  };
  
  handleChange = input => {
    const { name , value } = input.target;
    
    const data = {...this.state.data};
    data[name] = value;
    
    const errors = {...this.state.errors};
    const errorMessage = this.onChangeValidation(input);
    if (errorMessage) errors[name] = errorMessage;
    else delete errors[name];
    
    this.setState({ data, errors })
  }

  handleSubmit = e => {
    e.preventDefault();
    const errors = this.onSubmitValidation();
    this.setState({ errors: errors || {} });
    if ( errors ) return;

    this.doSubmit();
  };
}
