import React from 'react'

export default function Like({ like, onLike}) {
  let heartClass = "bi bi-heart";
  if (like) heartClass += '-fill';

  return (
    <i 
      onClick={onLike}
      className={heartClass} 
      style={{ cursor : 'pointer' }}
    ></i>
  )
}
