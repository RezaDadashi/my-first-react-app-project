import React, { useState } from 'react'

export default function UpdatePost({ updatePost, posts, ...props }) {

  const id = props.match.params.id;
  // const originalPostTitle = posts.filter(p => p.id == updatedPostObj.id)[0];

  const [postTitle, setPostTitle] = useState('');
  const [postBody, setPostBody] = useState('');

  const handleSubmitUpdate = e => {
    e.preventDefault();
    props.history.push('/posts');
    const updatedPostObj = { title: postTitle, body: postBody, id: id};
    updatePost(updatedPostObj);
  }

  return (
    <div className='container text-center w-50'>
      <h1 className='p-5'>Update post</h1>
      <form onSubmit={handleSubmitUpdate}>
        <div className="form-group">
          <label htmlFor="exampleInput1">Post Title</label>
          <input 
            value={postTitle}
            onChange={e => setPostTitle(e.target.value)}
            type="text" 
            autoFocus
            className="form-control" 
            id="exampleInput1"/>
        </div>
        <div className="form-group">
          <label htmlFor="exampleInput2">Post Body</label>
          <textarea
            value={postBody} 
            onChange={e => setPostBody(e.target.value)}
            className="form-control" 
            id="exampleInput2" 
            rows="4">
          </textarea>
        </div>
        
        <button className="btn btn-primary m-5 px-5">Update Post</button>
      </form>
    </div>
  )
}
