import React from 'react'

export default function Category({ categories, onClick }) {
  
  return (
    <ul className="list-group">
      {categories.map(category => 
        <li 
          key={category.title} 
          className={category.active ? "list-group-item active" : "list-group-item"}
          style={{ cursor: 'pointer' }}
          onClick={() => onClick(category)}
        >
          {category.title}
        </li>
      )}
    </ul>
  )
}
