import React from 'react'
import { Link } from 'react-router-dom'
import Like from './like';

export default function Table({ posts, start, isLoading, onLike, onDelete, onSort, sortOrder }) {

  let sortIconClassName = 'ml-3 bi bi-caret-';
  sortIconClassName += sortOrder === 'asc' ? 'up-fill' : 'down-fill';

  return (
    <table className="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th 
          scope="col"
          onClick={onSort} 
          style={{ cursor:'pointer' }}
        >
          Post Title <i className={sortIconClassName}></i>
        </th>
        <th className='text-center' scope="col">Like</th>
        <th className='text-center' scope="col">Update</th>
        <th className='text-center' scope="col">Delete</th>
      </tr>
    </thead>
    {isLoading 
      ? <tbody>
        <tr>
          <td>
            <p className='h3 text-warning mt-5'>Conecting to the Server...</p>
          </td>
        </tr>
        </tbody>
      : <tbody>
          {posts.map((post, index) =>
            <tr key={post.id}>
              <th scope="row">{index+start+1}</th>
              <td>{post.title}</td>
              <td className='text-center'>
                <Like onLike={() => onLike(post)} like={post.like}/>
              </td>
              <td className='text-center'>
                <Link to={`/posts/${post.id}`}>
                  <button
                    className="btn btn-outline-primary"
                  >
                    Update
                  </button>
                </Link>
              </td>
              <td className='text-center'>
                <button 
                  className="btn btn-outline-danger"
                  onClick={() => onDelete(post)}
                >
                  Delete
                </button>
              </td>
            </tr>)}
        </tbody>
    }
  </table>
)
}
