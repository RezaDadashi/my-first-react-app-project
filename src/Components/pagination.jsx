import React from 'react'
import _ from 'lodash';

export default function Pagination({ length, pageSize, currentPage, onClick }) {

  let pages = _.range(1, Math.ceil(length / pageSize) + 1);
  if (pages.length === 1) pages = [];

  return (
    <ul className="pagination pagination-sm mt-4">
      {/* <li className="page-item disabled">
        <a className="page-link" href="#1" aria-disabled="true">Previous</a>
      </li> */}
      {
        pages.map((page, i) => 
          <li 
            onClick={() => onClick(i+1)} 
            key={page}
            style={{ cursor: 'pointer'}} 
            className={currentPage === (i+1) ? "page-item active" : "page-item"}>
              <div className="page-link">{page}</div>
          </li>
        )
      }
      {/* <li className="page-item">
        <a className="page-link" href="#1">Next</a>
      </li> */}
    </ul>
  )
}
