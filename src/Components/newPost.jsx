import React, { useState } from 'react'

export default function NewPost({ addNewPost, ...props }) {

  const [postTitle, setPostTitle] = useState('');
  const [postBody, setPostBody] = useState('');

  const handleSubmitNewPost = e => {
    e.preventDefault();
    props.history.push('/posts');
    const newPostObj = { title: postTitle, body: postBody};
    addNewPost(newPostObj);
  }

  return (
    <div className='container text-center w-50'>
      <h1 className='p-5'>Add New post</h1>
      <form onSubmit={handleSubmitNewPost}>
        <div className="form-group">
          <label htmlFor="exampleInput1">Post Title</label>
          <input 
            onChange={e => setPostTitle(e.target.value)}
            type="text" 
            autoFocus
            className="form-control" 
            id="exampleInput1"/>
        </div>
        <div className="form-group">
          <label htmlFor="exampleInput2">Post Body</label>
          <textarea 
            onChange={e => setPostBody(e.target.value)}
            className="form-control" 
            id="exampleInput2" 
            rows="4">
          </textarea>
        </div>
        
        <button className="btn btn-primary m-5 px-5">Add Post</button>
      </form>
    </div>
  )
}
