import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Login from './Components/login';
import Register from './Components/register';
import Navbar from './Components/navbar';
import NotFound from './Components/notFound';
import Posts from './Components/posts';
import './App.css';


function App() {

  return (
    <div>
      <Navbar />
      <Switch>
        <Route path='/login' component={Login}/>
        <Route path='/register' component={Register}/>
        <Route path='/not-found' component={NotFound}/>
        <Route path='/posts' component={Posts}/>
        <Redirect from='/' to='/posts' />
        <Redirect to='not-found' />
      </Switch>
    </div>
  );
}

export default App;
